<?php

namespace Drupal\hfc_assessment_reports;

use Drupal\Component\Utility\Html;

/**
 * Defines the PLO IDC Mapping Helper class.
 *
 * @package Drupal\hfc_assessment_reports
 */
class PloHelper implements PloHelperInterface {

  /**
   * {@inheritdoc}
   */
  public static function explodeOutcomes(array $outcomes): array {

    $mappings = [];
    $course_names = [];
    $assessment_years = [];

    foreach ($outcomes as $delta => $item) {
      $mappings[$delta]['type'] = $item->get('field_outcome_type')->value;
      $mappings[$delta]['outcome'] = $item->get('field_plo_single')->value;

      foreach ($item->get('field_plo_required_courses')->referencedEntities() as $required_course) {
        $rqc = $required_course->get('field_plo_required_course')->referencedEntities();
        $rqc = reset($rqc);

        if ($course_master = $rqc->getMaster() ?? $course_master = $rqc->getProposal()) {
          if ($course_master->getType() == 'pseudo_course') {
            $course_name = self::shorten($course_master->label());
          }
          else {
            $course_name = $course_master->get('field_crs_subject')->target_id . '-' . $course_master->get('field_crs_number')->value;
          }

          $course_names[$course_name] = Html::escape($course_name);
          foreach ($required_course->get('field_plo_code')->getValue() as $code) {
            $mappings[$delta]['courses'][$course_name][] = $code['value'];
          }
        }
        else {
          // Must be static. We are not in object context.
          \Drupal::messenger()->addError(t(
            'Cannot retrieve Course Master for RQC #@id: @title!',
            ['@id' => $rqc->id(), '@title' => $rqc->label()]
          ));
        }
      }
      if ($year = $item->get('field_outcome_review_year')->value) {
        $assessment_years[$year] = $year;
        $mappings[$delta]['year'] = $year;
      }
    }

    // Sort courses and years so they display in correct column order.
    ksort($course_names);
    ksort($assessment_years);

    $plo_rows = [];
    $ilo_rows = [];

    foreach ($mappings as $delta => $outcome) {
      $row = [
        'outcome' => [
          'data' => $outcome['outcome'],
          'class' => ['outcome-description'],
        ],
      ];

      foreach ($course_names as $course_name) {
        $row[$course_name] = [
          'data' => !empty($outcome['courses'][$course_name]) ? implode(', ', $outcome['courses'][$course_name]) : NULL,
          'class' => ['outcome-course-mapping'],
        ];
      }

      foreach ($assessment_years as $year) {
        $row["review-{$year}"] = [
          'data' => (($outcome['year'] ?? NULL) == $year) ? "X" : NULL,
          'class' => ['outcome-review-year'],
        ];
      }

      if ($outcome['type'] == 'plo') {
        $plo_rows[] = $row;
      }
      elseif ($outcome['type'] == 'ilo') {
        $ilo_rows[] = $row;
      }
    }

    $year_labels = array_map(function ($year) {
      $start = $year - 1;
      $end = str_pad($year % 100, 2, '0', STR_PAD_LEFT);
      return [
        'data' => "{$start}/{$end}",
        'class' => ['outcome-review-year'],
      ];
    }, $assessment_years);

    $plo_table = [
      '#theme' => 'table',
      '#header' => ['outcome' => Html::escape('Program Outcomes')] + $course_names + $year_labels,
      '#rows' => $plo_rows,
      '#attributes' => ['class' => ['learning-outcome-map']],
      '#empty' => t('Nothing to display.'),
    ];

    $ilo_table = [
      '#theme' => 'table',
      '#header' => ['outcome' => Html::escape('Institutional Outcomes')] + $course_names + $year_labels,
      '#rows' => $ilo_rows,
      '#attributes' => ['class' => ['learning-outcome-map']],
      '#empty' => t('Nothing to display.'),
    ];

    return [
      'plo_table' => $plo_table,
      'ilo_table' => $ilo_table,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function shorten(string $title): string {

    if (preg_match('/^Cat1\./', $title)) {
      return 'Cat1.EnglishComp';
    }
    if (preg_match('/^Cat2\./', $title)) {
      return 'Cat2.EnglishComp';
    }
    if (preg_match('/^Cat3\./', $title)) {
      return 'Cat3.Math';
    }
    if (preg_match('/^Cat4\./', $title)) {
      return 'Cat4.SocialSci';
    }
    if (preg_match('/^Cat5\./', $title)) {
      return 'Cat5.Human';
    }
    if (preg_match('/^Cat6\./', $title)) {
      return 'Cat6.NatSci';
    }
    if (preg_match('/^Cat7\./', $title)) {
      return 'Cat7.GenEd';
    }
    $title = preg_replace('/^([A-Za-z0-9]*)(.*$)/', '$1', $title) . '.PC';
    return $title;
  }

}
