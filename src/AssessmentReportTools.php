<?php

namespace Drupal\hfc_assessment_reports;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;
use Drupal\node\NodeInterface;

/**
 * Defines the Assessment Report Tools Service.
 *
 * Tools for Assessment Reports.
 */
class AssessmentReportTools implements AssessmentReportToolsInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\hfc_catalog_helper\CatalogUtilitiesInterface definition.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $helper;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Constructs a new AssessmentReportTools object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The Database service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The Entity Field Manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The Account Proxy Interface service.
   * @param \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface $hfc_catalog_helper
   *   The Catalog Helper service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(
    Connection $database,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
    AccountProxyInterface $current_user,
    CatalogUtilitiesInterface $hfc_catalog_helper,
    TimeInterface $time
  ) {
    $this->database = $database;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->helper = $hfc_catalog_helper;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrograms() {

    $query = $this->database->select('related_program', 'p');
    $query->addField('p', 'id');
    $query->isNotNull('master_nid');
    $query->condition('degree_type', ['CERT', 'AREA', 'EA'], 'NOT IN');
    $query->condition('program_status', 'inactive', '<>');
    $query->orderBy('title');
    $ids = $query->execute()->fetchCol();
    return $this->entityTypeManager->getStorage('related_program')->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getProgramOptions() {
    $query = $this->database->select('related_program', 'p');
    $query->fields('p', ['id', 'title']);
    $query->isNotNull('master_nid');
    $query->condition('degree_type', ['CERT', 'AREA', 'EA'], 'NOT IN');
    $query->condition('program_status', 'inactive', '<>');
    $query->orderBy('title');
    return $query->execute()->fetchAllKeyed();
  }

  /**
   * {@inheritdoc}
   */
  public function getProgramLearningOutcomeOptions() {
    $query = $this->database->select('related_program', 'p');
    $query->fields('p', ['id', 'title', 'master_nid']);
    $query->leftJoin(
      'node__field_related_program', 'r',
      "p.id = r.field_related_program_target_id AND
       r.bundle = 'program_learning_outcome'"
    );
    $query->fields('r', ['entity_id', 'revision_id']);
    $query->condition('degree_type', ['CERT', 'AREA', 'EA'], 'NOT IN');
    $query->condition('program_status', 'inactive', '<>');
    $query->condition('r.revision_id', NULL, 'IS NULL');
    $query->orderBy('title');
    return array_map(function ($item) {
      $title = $item->title . (
        empty($item->master_nid) ? ' (Proposal)' : ''
      );
      return $title;
    }, $query->execute()->fetchAllAssoc('id'));
  }

  /**
   * {@inheritdoc}
   */
  public function getOffices() {
    $query = $this->database->select('node', 'n');

    $query->join('node_field_data', 'd', "n.vid = d.vid");
    $query->join('node__field_office_cclo', 'field_office_cclo', "n.vid = field_office_cclo.revision_id");

    $query->fields('n', ['nid', 'type']);
    $query->addfield('d', 'title');

    $query->condition('n.type', 'office');
    $query->condition('d.status', NodeInterface::PUBLISHED);
    $query->condition('field_office_cclo_value', 1);

    $query->orderBy('d.title');

    if ($result = $query->execute()->fetchAll()) {
      foreach ($result as $item) {
        $output[$item->nid] = Html::escape($item->title);
      }
      return $output;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getExisting(array $values) {
    if ($nids = $this->queryExisting($values)) {
      $nid = reset($nids);
      return $this->entityTypeManager->getStorage('node')->load($nid);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getAllExisting(array $values) {
    if ($nids = $this->queryExisting($values)) {
      return $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkExisting(array $values) {
    return !empty($this->queryExisting($values));
  }

  /**
   * Get existing report.
   *
   * @param array $values
   *   Form values submitted by the user.
   *
   * @return int[]
   *   Return matching nids.
   */
  private function queryExisting(array $values) {
    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', $values['report_node_type'])
      ->accessCheck(FALSE);

    if ($values['report_node_type'] != 'program_learning_outcome') {
      $query->condition('field_acyr_list', $values['program_year']);
    }

    if ($values['report_node_type'] == 'cocurricular_annual_report') {
      $query->condition('field_office_contact', $values['office_nid']);
      $query->condition('field_co_curricular_learning_out', $values['cclo']);
    }
    elseif ($values['report_node_type'] == 'institutional_outcome_assessment') {
      $query->condition('field_learning_outcome', $values['ilo']);
    }
    elseif ($values['report_node_type'] == 'program_assessment_institutional') {
      $query->condition('field_related_program', $values['related_program']);
      if (!empty($values['ilo'])) {
        $query->condition('field_learning_outcome', $values['ilo']);
      }
    }
    else {
      $query->condition('field_related_program', $values['related_program']);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getCcloTypes() {
    $outcome_types = &drupal_static(__FUNCTION__);
    if (!isset($outcome_types)) {
      $program_fields = $this->entityFieldManager->getFieldDefinitions('node', 'cocurricular_annual_report');
      $outcome_types = $program_fields['field_co_curricular_learning_out']->getSetting('allowed_values');
    }
    return $outcome_types;
  }

  /**
   * {@inheritdoc}
   */
  public function getIloTypes() {
    $outcome_types = &drupal_static(__FUNCTION__);
    if (!isset($outcome_types)) {
      $program_fields = $this->entityFieldManager->getFieldDefinitions('node', 'institutional_outcome_assessment');
      $outcome_types = $program_fields['field_learning_outcome']->getSetting('allowed_values');

      // Trim outcomes to make them readable.
      foreach ($outcome_types as $key => $value) {
        list($trimmed) = preg_split('/:/', $value);
        $outcome_types[$key] = $trimmed;
      }
    }
    return $outcome_types;
  }

  /**
   * {@inheritdoc}
   */
  public function createReport(array $values) {

    $node = $this->entityTypeManager->getStorage('node')->create([
      'type' => $values['report_node_type'],
      'language' => Language::LANGCODE_NOT_SPECIFIED,
      'created' => $this->time->getRequestTime(),
      'changed' => $this->time->getRequestTime(),
      'status' => NodeInterface::PUBLISHED,
      'moderation_state' => 'published',
      'uid' => $this->currentUser->id(),
      'name' => $this->currentUser->getAccountName(),
    ]);

    if ($values['report_node_type'] == 'cocurricular_annual_report') {
      $node->field_office_contact->target_id = $values['office_nid'];
      $node->field_co_curricular_learning_out->value = $values['cclo'];

      $office = $this->entityTypeManager->getStorage('node')->load($values['office_nid']);

      $cclo_label = $this->getCcloTypes()[$values['cclo']];

      $title = implode(' ', [
        $program_year,
        $this->getNodeTypeLabel($values['report_node_type']),
        explode(':', $cclo_label)[0],
        '(' . $office->label() . ')',
      ]);
    }
    elseif ($values['report_node_type'] == 'institutional_outcome_assessment') {
      $node->field_learning_outcome->value = $values['ilo'];
      $ilo_label = $this->getIloTypes()[$values['ilo']];

      $title = implode(' ', [
        $program_year,
        $ilo_label,
        $this->getNodeTypeLabel($values['report_node_type']),
      ]);
    }
    elseif ($values['report_node_type'] == 'program_assessment_institutional') {
      $node->field_related_program->target_id = $values['related_program'];
      $node->field_learning_outcome->value = $values['ilo'];

      $program = $this->entityTypeManager->getStorage('related_program')->load($values['related_program']);

      $ilo_label = $this->getIloTypes()[$values['ilo']];

      $title = implode(' ', [
        $program_year,
        $ilo_label,
        $this->getNodeTypeLabel($values['report_node_type']),
        'for',
        $program->label(),
      ]);
    }
    elseif ($values['report_node_type'] == 'program_learning_outcome') {
      $node->field_related_program->target_id = $values['related_program'];

      $program = $this->entityTypeManager->getStorage('related_program')->load($values['related_program']);

      // @todo Update getProgramOptions() above (and the form builder)
      // to allow proposal-only related_program values to be selected here.
      $program_master = $program->master_nid->entity ?? $program->proposal_nid->entity;

      $title = implode(' ', [
        'IDC mapping for',
        $program_master->field_program_name->value,
        '(', $program_master->field_program_type->value, ')',
      ]);

      $paragraphs = [];
      if ($outcomes = $program_master->field_program_learning_outcomes->getValue()) {
        foreach ($outcomes as $outcome) {
          $paragraph = $this->entityTypeManager->getStorage('paragraph')->create([
            'type' => 'program_learning_outcome',
            'field_plo_single' => $outcome,
            'field_outcome_type' => 'plo',
          ]);
          $paragraph->isNew();
          $paragraph->save();
          $paragraphs[] = [
            'target_id' => $paragraph->id(),
            'target_revision_id' => $paragraph->getRevisionId(),
          ];
        }
      }

      if ($outcomes = $this->entityTypeManager->getStorage('hfc_assessment_ilo')->loadMultiple()) {
        foreach ($outcomes as $outcome) {
          $paragraph = $this->entityTypeManager->getStorage('paragraph')->create([
            'type' => 'program_learning_outcome',
            'field_plo_single' => $outcome->label(),
            'field_outcome_review_year' => $outcome->get('assessment_year'),
            'field_outcome_type' => 'ilo',
          ]);
          $paragraph->isNew();
          $paragraph->save();
          $paragraphs[] = [
            'target_id' => $paragraph->id(),
            'target_revision_id' => $paragraph->getRevisionId(),
          ];
        }
      }

      if (!empty($paragraphs)) {
        $node->field_outcomes->setValue($paragraphs);
      }
    }
    else {
      $node->field_related_program->target_id = $values['related_program'];

      $program = $this->entityTypeManager->getStorage('related_program')->load($values['related_program']);

      $title = implode(' ', [
        $program_year,
        $this->getNodeTypeLabel($values['report_node_type']),
        'for',
        $program->label(),
      ]);
    }

    $this->messenger()->addMessage($this->t('Creating %title', ['%title' => $title]));

    $node->title = $title;

    if ($values['report_node_type'] != 'program_learning_outcome') {
      $program_year = strval($values['program_year'] - 1) . '-' . strval($values['program_year']);
      $node->field_acyr_list->setValue($values['program_year']);
    }

    if ($values['report_node_type'] == 'program_assessment_annual_report') {
      if ($outcomes = $program->master_nid->entity->field_program_learning_outcomes->getValue()) {
        $paragraphs = [];
        foreach ($outcomes as $outcome) {
          $paragraph = $this->entityTypeManager->getStorage('paragraph')->create([
            'type' => 'prog_assess_annual_plo',
            'field_plo_single' => $outcome,
          ]);
          $paragraph->isNew();
          $paragraph->save();
          $paragraphs[] = [
            'target_id' => $paragraph->id(),
            'target_revision_id' => $paragraph->getRevisionId(),
          ];
        }
        $node->field_plo_annual_assessment->setValue($paragraphs);
      }
    }

    $node->isNew();
    return $node;
  }

  /**
   * Get a NodeType label value.
   *
   * @param string $type
   *   The machine name of the node type.
   *
   * @return string
   *   The node type label.
   */
  private function getNodeTypeLabel($type) {
    return $this->entityTypeManager->getStorage('node_type')->load($type)->label();
  }

}
