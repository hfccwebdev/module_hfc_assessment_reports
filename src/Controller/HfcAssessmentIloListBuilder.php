<?php

namespace Drupal\hfc_assessment_reports\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of HFC Assessment ILOs.
 */
class HfcAssessmentIloListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Machine name');
    $header['label'] = $this->t('Outcome');
    $header['description'] = $this->t('Description');
    $header['assessment_year'] = $this->t('Assessment Year');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    $row['label'] = $entity->label();
    $row['description'] = $entity->get('description');

    $year = $entity->get('assessment_year');
    $year = ($year - 1) . '/' . substr($year, -2);
    $row['assessment_year'] = $year;

    return $row + parent::buildRow($entity);
  }

}
