<?php

namespace Drupal\hfc_assessment_reports\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;

/**
 * Page controller for assessment report lists.
 *
 * @package Drupal\hfc_assessment_reports\Controller
 */
class AssessmentReportsController extends ControllerBase {

  /**
   * Drupal\Core\Form\FormBuilderInterface definition.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new DefaultController object.
   */
  public function __construct(FormBuilderInterface $form_builder) {
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder')
    );
  }

  /**
   * Generate new cocurricular_annual_report form.
   */
  public function cocurricularAssessmentAnnualReport() {
    return $this->formBuilder->getForm('Drupal\hfc_assessment_reports\Form\NodeCreateForm', 'cocurricular_annual_report');
  }

  /**
   * Generate new institutional_outcome_assessment form.
   */
  public function institutionalAssessmentAnnualReport() {
    return $this->formBuilder->getForm('Drupal\hfc_assessment_reports\Form\NodeCreateForm', 'institutional_outcome_assessment');
  }

  /**
   * Generate new program_assessment_annual_report form.
   */
  public function programAssessmentAnnualReport() {
    return $this->formBuilder->getForm('Drupal\hfc_assessment_reports\Form\NodeCreateForm', 'program_assessment_annual_report');
  }

  /**
   * Generate new program_assessment_institutional form.
   */
  public function programAssessmentInstitutional() {
    return $this->formBuilder->getForm('Drupal\hfc_assessment_reports\Form\NodeCreateForm', 'program_assessment_institutional');
  }

  /**
   * Generate new program_learning_oputcome form.
   */
  public function programLearningOutcome() {
    return $this->formBuilder->getForm('Drupal\hfc_assessment_reports\Form\NodeCreateForm', 'program_learning_outcome');
  }

}
