<?php

namespace Drupal\hfc_assessment_reports;

/**
 * Defines the Assessment Report Tools Interface.
 */
interface AssessmentReportToolsInterface {

  /**
   * Get available programs.
   *
   * @return \Drupal\hfc_related_program\Entity\RelatedProgram[]
   *   An array of Related Program Connectors, keyed by ID.
   */
  public function getPrograms();

  /**
   * Get an options list of available programs.
   *
   * @return array
   *   A keyed array of Related Program Connector titles.
   */
  public function getProgramOptions();

  /**
   * Get a filtered options list of available programs.
   *
   * This list is filtered for creating program_learning_outcome content.
   *
   * @return array
   *   A keyed array of Related Program Connector titles.
   */
  public function getProgramLearningOutcomeOptions();

  /**
   * Get cclo office list.
   */
  public function getOffices();

  /**
   * Get existing report.
   *
   * @param array $values
   *   Form values submitted by the user.
   *
   * @return \Drupal\node\Entity\NodeInterface
   *   Returns matching report if found.
   */
  public function getExisting(array $values);

  /**
   * Get multiple existing report.
   *
   * @param array $values
   *   Form values submitted by the user.
   *
   * @return \Drupal\node\Entity\NodeInterface[]
   *   Returns matching reports if found.
   */
  public function getAllExisting(array $values);

  /**
   * Check for existing reports.
   *
   * @param array $values
   *   Form values submitted by the user.
   *
   * @return bool
   *   Returns true if matching report is found.
   */
  public function checkExisting(array $values);

  /**
   * Get Co-Curricular Learning Outcome types.
   */
  public function getCcloTypes();

  /**
   * Get Institutional Learning Outcome types.
   */
  public function getIloTypes();

  /**
   * Create a new assessment report node.
   */
  public function createReport(array $values);

}
