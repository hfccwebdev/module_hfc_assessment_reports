<?php

namespace Drupal\hfc_assessment_reports\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\hfc_assessment_reports\PloHelper;
use Drupal\paragraphs\Plugin\Field\FieldFormatter\ParagraphsSummaryFormatter;

/**
 * Plugin implementation of the 'hfc_plo_grid' formatter.
 *
 * @FieldFormatter(
 *   id = "hfc_plo_grid",
 *   label = @Translation("PLO Mapping Grid"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class PloGridFormatter extends ParagraphsSummaryFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    return [PloHelper::explodeOutcomes($items->referencedEntities())];

  }

}
