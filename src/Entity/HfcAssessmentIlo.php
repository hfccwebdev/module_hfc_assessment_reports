<?php

namespace Drupal\hfc_assessment_reports\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Institutional Learning Outcome entity.
 *
 * @ConfigEntityType(
 *   id = "hfc_assessment_ilo",
 *   label = @Translation("Institutional Learning Outcome"),
 *   handlers = {
 *     "list_builder" = "Drupal\hfc_assessment_reports\Controller\HfcAssessmentIloListBuilder",
 *     "form" = {
 *       "add" = "Drupal\hfc_assessment_reports\Form\HfcAssessmentIloForm",
 *       "edit" = "Drupal\hfc_assessment_reports\Form\HfcAssessmentIloForm",
 *     }
 *   },
 *   config_prefix = "hfc_assessment_ilo",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "assessment_year"
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/hfc/assessment/ilo/{hfc_assessment_ilo}",
 *   }
 * )
 */
class HfcAssessmentIlo extends ConfigEntityBase {

  /**
   * The outcome ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The outcome label.
   *
   * @var string
   */
  protected $label;

  /**
   * The outcome full description.
   *
   * @var string
   */
  protected $description;

  /**
   * Date of next assessment for this outcome.
   *
   * @var string
   */
  protected $assessment_year;

}
