<?php

namespace Drupal\hfc_assessment_reports;

/**
 * Defines the PLO IDC Mapping Helper interface.
 *
 * @package Drupal\hfc_assessment_reports
 */
interface PloHelperInterface {

  /**
   * Explode PLO field_outcomes into grid data.
   *
   * @param \Drupal\paragraphs\ParagraphInterface[] $outcomes
   *   The outcomes to expand.
   *
   * @return array
   *   A renderable table.
   */
  public static function explodeOutcomes(array $outcomes): array;

  /**
   * Shorten course name for pseudo-courses.
   *
   * This is used for the PLO mapping table column headings.
   *
   * @param string $title
   *   The title to parse.
   *
   * @return string
   *   The parsed string.
   */
  public static function shorten(string $title): string;

}
