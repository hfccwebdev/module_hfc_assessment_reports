<?php

namespace Drupal\hfc_assessment_reports\Form;

/**
 * Defines the Assessment Program PLO Reporting Dashboard.
 */
class ProgramPloDashboardForm extends AssessmentDashboardBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hfc_assess_program_plo_dashboard';
  }

  /**
   * {@inheritdoc}
   */
  protected function buildReport($values) {

    $programs = $this->tools->getPrograms();

    $rows = [];
    foreach ($programs as $program) {
      $report = $this->tools->getExisting([
        'report_node_type' => 'program_assessment_annual_report',
        'related_program' => $program->id(),
        'program_year' => $values['program_year'],
      ]);

      $title = $this->programInfo($program, $report);
      $rows[] = $this->buildRow($title, $report);
    }
    $this->has_outcomes_column = FALSE;
    return $this->outputTable($rows);
  }

  /**
   * {@inheritdoc}
   */
  protected function buildRow($title, $report) {

    $created = $report
      ? $this->dateFormatter->format($report->getCreatedTime(), 'custom', 'n/j/Y')
      : NULL;
    $field_date_casl_feedback = !empty($report->field_date_casl_feedback->date)
      ? $report->field_date_casl_feedback->date->format('n/j/Y')
      : NULL;
    $field_date_action_plan = !empty($report->field_date_action_plan->date)
      ? $report->field_date_action_plan->date->format('n/j/Y')
      : NULL;
    $field_date_close_loop = !empty($report->field_date_close_loop->date)
      ? $report->field_date_close_loop->date->format('n/j/Y')
      : NULL;
    $field_date_casl_share = !empty($report->field_date_casl_share->date)
      ? $report->field_date_casl_share->date->format('n/j/Y')
      : NULL;

    return [
      $title,
      $created,
      $field_date_casl_feedback,
      $field_date_action_plan,
      $field_date_close_loop,
      $field_date_casl_share,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getTableHeader() {
    return [
      $this->t('Program'),
      $this->t('Created'),
      $this->t('CASL Feedback'),
      $this->t('Action Plan'),
      $this->t('Close Loop'),
      $this->t('CASL Share'),
    ];
  }

}
