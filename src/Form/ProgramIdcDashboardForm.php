<?php

namespace Drupal\hfc_assessment_reports\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the Assessment Program Course IDC Reporting Dashboard.
 */
class ProgramIdcDashboardForm extends AssessmentDashboardBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hfc_assess_program_idc_dashboard';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node_type = NULL) {

    // We don't actually need a form for this report, but
    // we want to reuse code from the base form, so we will
    // override this method to remove all actual form fields.
    $values = [];
    $form['report'] = $this->buildReport($values) + ['#weight' => 99];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildReport($values) {

    $programs = $this->tools->getPrograms();

    $rows = [];
    foreach ($programs as $program) {
      $report = $this->tools->getExisting([
        'report_node_type' => 'program_learning_outcome',
        'related_program' => $program->id(),
      ]);

      $title = $this->programInfo($program, $report);
      $rows[] = $this->buildRow($title, $report);
    }
    $this->has_outcomes_column = FALSE;
    return $this->outputTable($rows);
  }

  /**
   * {@inheritdoc}
   */
  protected function buildRow($title, $report) {

    $created = $report
      ? $this->dateFormatter->format($report->getCreatedTime(), 'custom', 'n/j/Y')
      : NULL;

    return [
      $title,
      $created,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getTableHeader() {
    return [
      $this->t('Program'),
      $this->t('Created'),
    ];
  }

}
