<?php

namespace Drupal\hfc_assessment_reports\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the HFC Assessment ILO add and edit forms.
 */
class HfcAssessmentIloForm extends EntityForm {

  /**
   * Constructs an ILO Form object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t("Label for the ILO."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$entity->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $entity->get('description'),
      '#rows' => 5,
      '#description' => $this->t("Full description of this outcome."),
      '#required' => TRUE,
    ];

    $form['assessment_year'] = [
      '#type' => 'select',
      '#title' => $this->t('Assessment Year'),
      '#default_value' => $entity->get('assessment_year'),
      '#options' => hfc_catalog_helper_acyr_list(),
      '#description' => $this->t('Year when this outcome should be assessed.'),
      '#required' => TRUE,
    ];

    // You will need additional form elements for your custom properties.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = $entity->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label ILO created.', [
        '%label' => $entity->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label ILO updated.', [
        '%label' => $entity->label(),
      ]));
    }

    $form_state->setRedirect('entity.hfc_assessment_ilo.collection');
  }

  /**
   * Helper function to check whether this ILO configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('hfc_assessment_ilo')->getQuery()
      ->accessCheck(FALSE)
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
