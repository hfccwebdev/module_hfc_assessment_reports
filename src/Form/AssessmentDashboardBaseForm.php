<?php

namespace Drupal\hfc_assessment_reports\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hankdata\HankDataInterface;
use Drupal\hfc_assessment_reports\AssessmentReportToolsInterface;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;
use Drupal\hfc_related_program\Entity\RelatedProgram;
use Drupal\node\Entity\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Assessment Program PLO Reporting Dashboard.
 */
abstract class AssessmentDashboardBaseForm extends FormBase {

  /**
   * Drupal\Core\Datetime\DateFormatterInterface definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Drupal\hfc_assessment_reports\AssessmentReportToolsInterface definition.
   *
   * @var \Drupal\hfc_assessment_reports\AssessmentReportToolsInterface
   */
  protected $tools;

  /**
   * Drupal\hankdata\HankDataInterface definition.
   *
   * @var \Drupal\hankdata\HankDataInterface
   */
  protected $hankData;

  /**
   * Drupal\hfc_catalog_helper\CatalogHelperInterface definition.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogHelperInterface
   */
  protected $catalogHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('hfc_assessment_reports.tools'),
      $container->get('hankdata'),
      $container->get('hfc_catalog_helper')
    );
  }

  /**
   * Constructs a new Assessment Dashboard Form object.
   */
  public function __construct(
    DateFormatterInterface $dateformatter,
    AssessmentReportToolsInterface $tools,
    HankDataInterface $hankdata,
    CatalogUtilitiesInterface $catalog_helper
  ) {
    $this->dateFormatter = $dateformatter;
    $this->tools = $tools;
    $this->hankData = $hankdata;
    $this->catalogHelper = $catalog_helper;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node_type = NULL) {

    $form['program_year'] = [
      '#type' => 'select',
      '#title' => $this->t('Academic Year'),
      '#options' => hfc_catalog_helper_acyr_list(),
      '#default_value' => $this->getCurrentAcYr(),
      '#required' => TRUE,
    ];

    $values = $form_state->getValues();
    if (empty($values['program_year'])) {
      $values['program_year'] = $this->getCurrentAcYr();
    }
    $form['report'] = $this->buildReport($values) + ['#weight' => 99];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Display'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * Build the requested report.
   */
  abstract protected function buildReport($program_year);

  /**
   * Build report row.
   *
   * @param string $title
   *   First column of the report.
   * @param \Drupal\node\Entity\NodeInterface $report
   *   The report node.
   */
  abstract protected function buildRow($title, NodeInterface $report);

  /**
   * Retrieve the table header array.
   */
  abstract protected function getTableHeader();

  /**
   * Build the output render array.
   */
  protected function outputTable($rows) {
    return [
      '#type' => 'table',
      '#header' => $this->getTableHeader(),
      '#rows' => $rows,
      '#sticky' => TRUE,
      '#attributes' => ['class' => ['assessment-dashboard-overview']],
      '#empty' => $this->t('Failed to generate output table'),
    ];
  }

  /**
   * Get the current Academic Year value.
   */
  protected function getCurrentAcYr() {
    $term_id = $this->hankData->getHankCurrentTerm();
    $matches = [];
    if (preg_match("/^(..)\/(..)$/", $term_id, $matches)) {
      $year = intval(intval(($matches[1]) > 75) ? "19" . $matches[1] : "20" . $matches[1]);
      if ($matches[2] == 'FA') {
        return strval($year);
      }
      elseif (in_array($matches[2], ['WI', 'SP', 'SU'])) {
        return strval($year - 1);
      }
    }
  }

  /**
   * Builds an inline template for the program name column.
   *
   * @param \Drupal\hfc_related_program\Entity\RelatedProgram $program
   *   The Related Program Connector.
   * @param \Drupal\node\Entity\NodeInterface|null $report
   *   The report node, if one exists.
   *
   * @return array
   *   An array describing a renderable table cell.
   */
  protected function programInfo(RelatedProgram $program, $report): array {

    // Get Faculty contact from Supplemental Program Info.
    if ($supplemental = $program->supp_nid->entity) {
      $contact = !empty($supplemental->field_faculty_contact->entity)
        ? $supplemental->field_faculty_contact->entity->label()
        : NULL;
    }
    else {
      $contact = NULL;
    }

    // Build the program info content.
    $program_name = $program->label();
    $info = [
      'name' => $report ? $report->toLink($program_name)->toString() : $program_name,
      'contact' => $contact,
    ];

    // Build an inline Twig template with the program info.
    $output = [
      '#type' => 'inline_template',
      '#template' => '{{ name }}<br>{{ contact }}',
      '#context' => $info,
    ];

    // Return as table cell.
    return [
      'data' => $output,
      'class' => ['program-info'],
    ];
  }

}
