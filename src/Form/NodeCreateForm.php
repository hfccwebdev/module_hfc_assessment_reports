<?php

namespace Drupal\hfc_assessment_reports\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hfc_assessment_reports\AssessmentReportToolsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Override core node creation form with custom options.
 */
class NodeCreateForm extends FormBase {

  /**
   * Drupal\hfc_assessment_reports\AssessmentReportToolsInterface definition.
   *
   * @var \Drupal\hfc_assessment_reports\AssessmentReportToolsInterface
   */
  protected $tools;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hfc_assessment_reports.tools')
    );
  }

  /**
   * Constructs a new NodeCreateForm object.
   */
  public function __construct(AssessmentReportToolsInterface $tools) {
    $this->tools = $tools;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hfc_assess_node_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node_type = NULL) {

    $form['report_node_type'] = [
      '#type' => 'value',
      '#value' => $node_type,
    ];

    if ($node_type == 'cocurricular_annual_report') {
      $form['office_nid'] = [
        '#type' => 'select',
        '#title' => $this->t('Department'),
        '#options' => $this->tools->getOffices(),
        '#required' => TRUE,
      ];
      $form['cclo'] = [
        '#type' => 'select',
        '#title' => $this->t('Outcome to assess'),
        '#options' => $this->tools->getCcloTypes(),
        '#required' => TRUE,
      ];
    }
    elseif ($node_type == 'institutional_outcome_assessment') {
      $form['ilo'] = [
        '#type' => 'select',
        '#title' => $this->t('Outcome to assess'),
        '#options' => $this->tools->getIloTypes(),
        '#required' => TRUE,
      ];
    }
    elseif ($node_type == 'program_assessment_institutional') {
      $form['related_program'] = [
        '#type' => 'select',
        '#title' => $this->t('Program Name'),
        '#options' => $this->tools->getProgramOptions(),
        '#required' => TRUE,
      ];
      $form['ilo'] = [
        '#type' => 'select',
        '#title' => $this->t('Outcome to assess'),
        '#options' => $this->tools->getIloTypes(),
        '#required' => TRUE,
      ];
    }
    elseif ($node_type == 'program_learning_outcome') {
      $form['related_program'] = [
        '#type' => 'select',
        '#title' => $this->t('Program Name'),
        '#options' => $this->tools->getProgramLearningOutcomeOptions(),
        '#required' => TRUE,
        '#description' => $this->t('
          This list contains existing and new programs, filtered to exclude
          content that already exists.<br>If you cannot find your program here,
          please check the assessment dashboard for existing content.
        '),
      ];
    }
    else {
      $form['related_program'] = [
        '#type' => 'select',
        '#title' => $this->t('Program Name'),
        '#options' => $this->tools->getProgramOptions(),
        '#required' => TRUE,
      ];
    }

    if ($node_type !== 'program_learning_outcome') {
      $form['program_year'] = [
        '#type' => 'select',
        '#title' => $this->t('Academic Year'),
        '#options' => hfc_catalog_helper_acyr_list(),
        '#default_value' => date('Y'),
        '#required' => TRUE,
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if ($this->tools->checkExisting($form_state->getValues())) {
      $form_state->setErrorByName('program_year', $this->t('The selected report already exists for this year.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $node = $this->tools->createReport($form_state->getValues());
    if (is_object($node) && $node->save()) {
      $form_state->setRedirect('entity.node.edit_form', ['node' => $node->id()]);
    }
    else {
      $this->messenger()->addMessage($this->t('Could not create requested content.'));
    }
  }

}
