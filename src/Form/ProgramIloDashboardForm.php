<?php

namespace Drupal\hfc_assessment_reports\Form;

/**
 * Defines the Assessment Program ILO Reporting Dashboard.
 */
class ProgramIloDashboardForm extends AssessmentDashboardBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hfc_assess_program_ilo_dashboard';
  }

  /**
   * {@inheritdoc}
   */
  protected function buildReport($values) {

    $programs = $this->tools->getPrograms();

    $rows = [];
    foreach ($programs as $program) {
      if ($reports = $this->tools->getAllExisting([
        'report_node_type' => 'program_assessment_institutional',
        'related_program' => $program->id(),
        'program_year' => $values['program_year'],
      ])) {
        foreach ($reports as $report) {
          $title = $this->programInfo($program, $report);
          $rows[] = $this->buildRow($title, $report);
        }
      }
      else {
        $title = $this->programInfo($program, []);
        $rows[] = $this->buildRow($title, []);
      }
    }
    $this->has_outcomes_column = TRUE;
    return $this->outputTable($rows);
  }

  /**
   * {@inheritdoc}
   */
  protected function buildRow($title, $report) {

    $outcome = $report
      ? $this->tools->getIloTypes()[$report->field_learning_outcome->value]
      : NULL;
    $created = $report
      ? $this->dateFormatter->format($report->getCreatedTime(), 'custom', 'n/j/Y')
      : NULL;
    $field_date_casl_feedback = !empty($report->field_date_casl_feedback->date)
      ? $report->field_date_casl_feedback->date->format('n/j/Y')
      : NULL;
    $field_date_action_plan = !empty($report->field_date_action_plan->date)
      ? $report->field_date_action_plan->date->format('n/j/Y')
      : NULL;
    $field_date_close_loop = !empty($report->field_date_close_loop->date)
      ? $report->field_date_close_loop->date->format('n/j/Y')
      : NULL;
    $field_date_casl_share = !empty($report->field_date_casl_share->date)
      ? $report->field_date_casl_share->date->format('n/j/Y')
      : NULL;

    return [
      $title,
      $outcome,
      $created,
      $field_date_casl_feedback,
      $field_date_action_plan,
      $field_date_close_loop,
      $field_date_casl_share,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getTableHeader() {
    return [
      $this->t('Program'),
      $this->t('Outcome'),
      $this->t('Created'),
      $this->t('CASL Feedback'),
      $this->t('Action Plan'),
      $this->t('Close Loop'),
      $this->t('CASL Share'),
    ];
  }

}
