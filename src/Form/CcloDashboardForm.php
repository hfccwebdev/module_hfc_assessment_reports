<?php

namespace Drupal\hfc_assessment_reports\Form;

/**
 * Defines the Assessment CCLO Reporting Dashboard.
 */
class CcloDashboardForm extends AssessmentDashboardBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hfc_assess_cclo_dashboard';
  }

  /**
   * {@inheritdoc}
   */
  protected function buildReport($values) {

    $types = $this->tools->getCcloTypes();
    $offices = $this->tools->getOffices();

    $output = [];
    foreach ($types as $type => $outcome) {
      $output[] = [
        '#prefix' => '<h2 class="title>',
        '#markup' => $this->t($outcome),
        '#suffix' => '</h2>',
      ];
      $rows = [];

      foreach ($offices as $office_nid => $title) {
        $report = $this->tools->getExisting([
          'report_node_type' => 'cocurricular_annual_report',
          'cclo' => $type,
          'office_nid' => $office_nid,
          'program_year' => $values['program_year'],
        ]);
        $rows[] = $this->buildRow($title, $report);
      }
      $output[] = $this->outputTable($rows);
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildRow($title, $report) {

    $created = $report
      ? $this->dateFormatter->format($report->getCreatedTime(), 'custom', 'n/j/Y')
      : NULL;
    $field_date_casl_feedback = !empty($report->field_date_casl_feedback->date)
      ? $report->field_date_casl_feedback->date->format('n/j/Y')
      : NULL;
    $field_date_action_plan = !empty($report->field_date_action_plan->date)
      ? $report->field_date_action_plan->date->format('n/j/Y')
      : NULL;
    $field_date_close_loop = !empty($report->field_date_close_loop->date)
      ? $report->field_date_close_loop->date->format('n/j/Y')
      : NULL;
    $field_date_casl_share = !empty($report->field_date_casl_share->date)
      ? $report->field_date_casl_share->date->format('n/j/Y')
      : NULL;

    return [
      $report ? $report->toLink($title)->toString() : $title,
      $created,
      $field_date_casl_feedback,
      $field_date_action_plan,
      $field_date_close_loop,
      $field_date_casl_share,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getTableHeader() {
    return [
      $this->t('Department'),
      $this->t('Created'),
      $this->t('CASL Feedback'),
      $this->t('Action Plan'),
      $this->t('Close Loop'),
      $this->t('CASL Share'),
    ];
  }

}
